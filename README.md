# lightmicroscopy
[![OntoFlow](https://gitlab.com/kupferdigital/process-graphs/lightmicroscopy/badges/main/pipeline.svg?style=flat-square&ignore_skipped=true&key_text=OntoFlow&key_width=80)](https://kupferdigital.gitlab.io/process-graphs/calphad)
[![Graph](https://gitlab.com/kupferdigital/process-graphs/lightmicroscopy/badges/main/pipeline.svg?style=flat-square&ignore_skipped=true&key_text=Turtle&key_width=80)](https://kupferdigital.gitlab.io/process-graphs/lightmicroscopy/index.ttl)

Light microscopy graph of the copper digital project.

<img src="https://kupferdigital.gitlab.io/process-graphs/lightmicroscopy/lightmicroscopy.svg">

['ttl.file'](https://kupferdigital.gitlab.io/process-graphs/lightmicroscopy/index.ttl)
